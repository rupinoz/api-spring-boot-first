package com.rupinoz.spring.object;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RupinozResponseObject {
    private String status;
    private String code;
    private IndexDataResponseObject data = new IndexDataResponseObject();
}
