package com.rupinoz.spring.object;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexResponseObject {
    private String status;
    private String code;
    private IndexDataResponseObject data = new IndexDataResponseObject();
}
