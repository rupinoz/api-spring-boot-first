package com.rupinoz.spring.object;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseObject {
    private String status;
    private String code;
    private UserDataResponseObject data = new UserDataResponseObject();
}
