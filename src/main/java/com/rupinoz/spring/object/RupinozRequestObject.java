package com.rupinoz.spring.object;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RupinozRequestObject {
    private String amount;
    private String email;
    private String name;
    private String msisdn;
    private String merchant;
    private String merchantid;
    private String respURL;
}
