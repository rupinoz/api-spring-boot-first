package com.rupinoz.spring.object;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserDataResponseObject {
    private int id;
    private String user;
    private String password;
    private String name;
    private int status;
    private String message;
}
