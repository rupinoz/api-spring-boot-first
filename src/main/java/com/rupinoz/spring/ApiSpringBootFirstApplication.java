package com.rupinoz.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSpringBootFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiSpringBootFirstApplication.class, args);
    }

}
