package com.rupinoz.spring.controller;

import com.rupinoz.spring.datasource.dto.UserProductDataObject;
import com.rupinoz.spring.datasource.dto.UserProductResponseObject;
import com.rupinoz.spring.datasource.entity.User;
import com.rupinoz.spring.object.UserResponseObject;
import com.rupinoz.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/user")
    public ResponseEntity<UserResponseObject> findUser(@RequestBody User user, @RequestHeader Map<String, String> headers) {
        ResponseEntity<UserResponseObject> responseEntity;
        HttpStatus status;
        UserResponseObject userResponseObject = service.getUser(user);
        if (userResponseObject.getCode().equals("00")){
            status = HttpStatus.OK;
        }else{
            status = HttpStatus.BAD_REQUEST;
        }
        responseEntity = new ResponseEntity<UserResponseObject>(userResponseObject, status);
        return responseEntity;
    }

    @PostMapping("/add/user")
    public UserResponseObject addUser(@RequestBody User user) {
        return service.saveUser(user);
    }

    @PostMapping("/add/users")
    public List<User> addUsers(@RequestBody List<User> users) {
        return service.saveUsers(users);
    }

    @GetMapping("/users")
    public List<User> findAllUsers() { return service.getUsers(); }

    @GetMapping("/user/{id}")
    public User findUserById(@PathVariable int id) { return service.getUserById(id); }

    @PutMapping("/update/user")
    public User updateUser(@RequestBody User user) {
        return service.updateUser(user);
    }

    @DeleteMapping("/delete/user/{id}")
    public String deleteUser(@PathVariable int id) {
        return service.deleteUser(id);
    }

    @GetMapping("/join")
    public ResponseEntity<List<UserProductDataObject>> getUserProductJoin() {
        return new ResponseEntity<List<UserProductDataObject>>(service.getUserProductJoin(), HttpStatus.OK);
    }

    @PostMapping("/join2")
    public ResponseEntity<UserProductResponseObject> getUserProductJoin2(@RequestBody User user, @RequestHeader Map<String, String> headers) {
        ResponseEntity<UserProductResponseObject> responseEntity;
        HttpStatus status;
        UserProductResponseObject userProductResponseObject = service.getUserProductJoin2(user);
        if (userProductResponseObject.getCode().equals("00")){
            status = HttpStatus.OK;
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        responseEntity = new ResponseEntity<UserProductResponseObject>(userProductResponseObject, status);
        return responseEntity;
    }
}
