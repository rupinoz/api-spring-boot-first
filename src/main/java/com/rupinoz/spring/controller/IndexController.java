package com.rupinoz.spring.controller;

import com.rupinoz.spring.object.*;
import com.rupinoz.spring.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class IndexController {

    @Autowired
    private IndexService service;

    @GetMapping("/")
    public ResponseEntity<String> getIndex() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return new ResponseEntity<>("{\n" +
                "    \"status\":\"Success\",\n" +
                "    \"code\":\"00\",\n" +
                "    \"data\":{\n" +
                "      \"service\":\"Spring-Boot API Ready\",\n" +
                "      \"author\":\"Rupinoz\"\n" +
                "    }\n" +
                "}", headers, HttpStatus.OK);
    }

    @PostMapping("/")
    public IndexResponseObject postIndex(@RequestBody IndexRequestObject index, @RequestHeader Map<String, String> headers) {
//        headers.forEach((key, value) -> System.out.println(key+": "+value));
//        System.out.println("Content-Type: "+headers.get("content-type"));
        IndexResponseObject indexResponseObject = service.postIndex(index);
        return indexResponseObject;
    }

    @PostMapping("/rupinoz")
    public ResponseEntity<RupinozResponseObject> postRupinoz(@RequestBody RupinozRequestObject rupinozRequestObject, @RequestHeader Map<String, String> headers) {
        ResponseEntity<RupinozResponseObject> responseEntity;
        HttpStatus status;
        RupinozResponseObject rupinozResponseObject = service.postRupinoz(rupinozRequestObject, headers);
        if (rupinozResponseObject.getCode().equals("00")){
            status = HttpStatus.OK;
        }else{
            status = HttpStatus.BAD_REQUEST;
        }
        responseEntity = new ResponseEntity<RupinozResponseObject>(rupinozResponseObject, status);
        return responseEntity;
    }

    @GetMapping("/rupinoz")
    public ResponseEntity<RupinozResponseObject> getRupinoz(@RequestBody RupinozRequestObject rupinozRequestObject, @RequestHeader Map<String, String> headers) {
        ResponseEntity<RupinozResponseObject> responseEntity;
        HttpStatus status;
        RupinozResponseObject rupinozResponseObject = service.getRupinoz(rupinozRequestObject);
        if (rupinozResponseObject.getCode().equals("00")){
            status = HttpStatus.OK;
        }else{
            status = HttpStatus.BAD_REQUEST;
        }
        responseEntity = new ResponseEntity<RupinozResponseObject>(rupinozResponseObject, status);
        return responseEntity;
    }

}
