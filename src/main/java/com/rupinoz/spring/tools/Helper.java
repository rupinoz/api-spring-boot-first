package com.rupinoz.spring.tools;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Helper {
    public static String Bcrypt(String value){
        String encodedPassword = "";
        try {
            int strength = 10;
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
            encodedPassword = bCryptPasswordEncoder.encode(value);
            return encodedPassword;
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            e.printStackTrace();
            return encodedPassword;
        }
    }

    public static String Md5(String value){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(value.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
