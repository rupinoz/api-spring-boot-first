package com.rupinoz.spring.datasource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProductResponseObject {
    private String status;
    private String code;
    private List<UserProductDataObject> data;
}
