package com.rupinoz.spring.datasource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserProductDataObject {

    private String usUser;
    private String usName;
    private String pdName;
    private double pdPrice;
    private String message;

    public UserProductDataObject(String usUser, String usName, String pdName, double pdPrice) {
        this.usUser = usUser;
        this.usName = usName;
        this.pdName = pdName;
        this.pdPrice = pdPrice;
    }

//    @Override
//    public String toString() {
//        return "{usUser=" + usUser + ", usName=" + usName + ", pdName=" + pdName + ", pdPrice=" + pdPrice + "}";
//    }

}
