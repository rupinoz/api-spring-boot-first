package com.rupinoz.spring.datasource.repository;

import com.rupinoz.spring.datasource.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
    Product findByName(String name);
}
