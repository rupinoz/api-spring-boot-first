package com.rupinoz.spring.datasource.repository;

import com.rupinoz.spring.datasource.dto.UserProductDataObject;
import com.rupinoz.spring.datasource.dto.UserProductResponseObject;
import com.rupinoz.spring.datasource.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUser(String user);
    User findByUserAndPassword(String user, String password);

    @Query("SELECT new com.rupinoz.spring.datasource.dto.UserProductDataObject(a.user, a.name, b.name, b.price) "
            + "FROM User a INNER JOIN a.products b")
    List<UserProductDataObject> findByUserProductJoin();

    @Query("SELECT new com.rupinoz.spring.datasource.dto.UserProductDataObject(a.user, a.name, b.name, b.price) "
            + "FROM User a INNER JOIN a.products b where a.status=:status")
    List<UserProductDataObject> findByUserProductJoin2(@Param("status") int status);

//    @Query(value = "SELECT * FROM tbl_user usr WHERE usr.user = :user", nativeQuery=true)
//    UserResponseObject findByUser(@Param("user") String user);
}
