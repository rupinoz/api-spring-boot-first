package com.rupinoz.spring.datasource.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_user")
public class User {

    @Id
    @GeneratedValue
    private int id;
    private String user;
    private String password;
    private String name;
    private int status;

    @OneToMany(targetEntity = Product.class, mappedBy = "id", orphanRemoval = false, fetch = FetchType.LAZY)
    private Set<Product> products;
}
