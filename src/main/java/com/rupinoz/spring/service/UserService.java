package com.rupinoz.spring.service;

import com.rupinoz.spring.datasource.dto.UserProductDataObject;
import com.rupinoz.spring.datasource.dto.UserProductResponseObject;
import com.rupinoz.spring.datasource.entity.User;
import com.rupinoz.spring.object.UserResponseObject;
import com.rupinoz.spring.datasource.repository.UserRepository;
import com.rupinoz.spring.tools.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;
    private Helper helper = new Helper();

    public UserResponseObject getUser(User user){
        UserResponseObject userResponseObject = new UserResponseObject();
        user = repository.findByUserAndPassword(user.getUser(), helper.Md5(user.getPassword()));
        if(user != null){
            userResponseObject.setStatus("Success");
            userResponseObject.setCode("00");
            userResponseObject.getData().setId(user.getId());
            userResponseObject.getData().setUser(user.getUser());
            userResponseObject.getData().setPassword(user.getPassword());
            userResponseObject.getData().setName(user.getName());
            userResponseObject.getData().setStatus(user.getStatus());
        }else{
            userResponseObject.setStatus("Failed");
            userResponseObject.setCode("F1");
            userResponseObject.getData().setMessage("Not Found");
        }
        return userResponseObject;
    }

    public UserResponseObject saveUser(User user){
        UserResponseObject userResponseObject = new UserResponseObject();
        User result = repository.findByUser(user.getUser());
        String hashPassword = helper.Md5(user.getPassword());
        user.setPassword(hashPassword);
        System.out.println(result);
        if(result != null){
            userResponseObject.setStatus("Failed");
            userResponseObject.setCode("F2");
            userResponseObject.getData().setMessage("Duplicated");
        }else{
            user = repository.save(user);
            userResponseObject.setStatus("Success");
            userResponseObject.setCode("00");
            userResponseObject.getData().setId(user.getId());
            userResponseObject.getData().setUser(user.getUser());
            userResponseObject.getData().setPassword(user.getPassword());
            userResponseObject.getData().setName(user.getName());
            userResponseObject.getData().setStatus(user.getStatus());
        }
//        System.out.println("Create user '"+user.getUser()+"'");
        return userResponseObject;
    }

    public List<User> saveUsers(List<User> users){
        return repository.saveAll(users);
    }

    public List<User> getUsers(){ return repository.findAll(); }

    public User getUserById(int id){ return repository.findById(id).orElse(null); }

    public String deleteUser(int id){
        repository.deleteById(id);
        return "User Remove !! "+id;
    }

    public User updateUser(User user){
        User existingUser=repository.findById(user.getId()).orElse(null);
        existingUser.setName(user.getName());
        existingUser.setStatus(user.getStatus());
        return repository.save(existingUser);
    }

    public List<UserProductDataObject> getUserProductJoin() {
        UserProductResponseObject user = new UserProductResponseObject();
        List<UserProductDataObject> list = repository.findByUserProductJoin();
        list.forEach(l -> System.out.println(l));
        return list;
    }

    public UserProductResponseObject getUserProductJoin2(User user) {
        UserProductResponseObject userProductResponseObject = new UserProductResponseObject();
        List<UserProductDataObject> list = repository.findByUserProductJoin2(user.getStatus());
//        list.forEach(l -> System.out.println(l));
        if(list == null || list.isEmpty()){
            List<UserProductDataObject> message = new ArrayList<UserProductDataObject>();
            UserProductDataObject userProductDataObject = new UserProductDataObject();
            userProductDataObject.setMessage("Not Available");
            message.add(userProductDataObject);

            userProductResponseObject.setStatus("Failed");
            userProductResponseObject.setCode("R1");
            userProductResponseObject.setData(message);
        }else{
            userProductResponseObject.setStatus("Success");
            userProductResponseObject.setCode("00");
            userProductResponseObject.setData(list);
        }
        return userProductResponseObject;
    }
}
