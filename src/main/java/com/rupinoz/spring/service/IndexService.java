package com.rupinoz.spring.service;

import com.rupinoz.spring.object.*;
import com.rupinoz.spring.datasource.repository.UserRepository;
import com.rupinoz.spring.tools.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class IndexService {

    @Autowired
    private UserRepository repository;
    private Helper helper = new Helper();

    public IndexResponseObject postIndex(IndexRequestObject index){
        IndexResponseObject indexResponseObject = new IndexResponseObject();
        indexResponseObject.setStatus("Success");
        indexResponseObject.setCode("00");
        indexResponseObject.getData().setService("Spring-Boot API Ready");
        indexResponseObject.getData().setAuthor("Rupinoz");
        return indexResponseObject;
    }

    public RupinozResponseObject postRupinoz(RupinozRequestObject rupinozRequestObject, Map<String, String> headers){
        RupinozResponseObject rupinozResponseObject;
        final String uri = "https://staging-gpn.kiselindonesia.net/";
        RestTemplate restTemplate = new RestTemplate();
        rupinozResponseObject = restTemplate.getForObject(uri, RupinozResponseObject.class);
        rupinozResponseObject.setStatus(rupinozResponseObject.getStatus());
        rupinozResponseObject.setCode("00");
        rupinozResponseObject.getData().setService(rupinozResponseObject.getData().getService());
        rupinozResponseObject.getData().setAuthor(rupinozResponseObject.getData().getAuthor());
        System.out.println(rupinozRequestObject.getEmail());
        System.out.println(headers.get("content-type"));
        System.out.println(headers.get("signature"));
        return rupinozResponseObject;
    }

    public RupinozResponseObject getRupinoz(RupinozRequestObject rupinozRequestObject){
        RupinozResponseObject rupinozResponseObject;
        final String uri = "https://staging-gpn.kiselindonesia.net/sue";
        RestTemplate restTemplate = new RestTemplate();
        rupinozResponseObject = restTemplate.getForObject(uri, RupinozResponseObject.class);
        rupinozResponseObject.setStatus(rupinozResponseObject.getStatus());
        rupinozResponseObject.setCode("00");
        rupinozResponseObject.getData().setService(rupinozResponseObject.getData().getService());
        rupinozResponseObject.getData().setAuthor(rupinozResponseObject.getData().getAuthor());
        return rupinozResponseObject;
    }

}
