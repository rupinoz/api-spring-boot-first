//package com.rupinoz.spring.exception;
//
//import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//import java.util.Map;
//
//@ControllerAdvice
//@RestController
//public class CustomizeResponseEntityExceptionHandler extends DefaultErrorAttributes {
//
////    @Override
////    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
////        Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);
////        errorAttributes.put("locale", webRequest.getLocale().toString());
////        errorAttributes.remove("error");
////
////        //...
////
////        return errorAttributes;
////    }
//
//    @ExceptionHandler(Throwable.class)
//    public final ResponseEntity<Object> handleAllExceptions(Throwable ex, WebRequest request) {
//        ExceptionResponse exceptionResponse = new ExceptionResponse("Failed", "F1", new ExceptionDataResponse());
//        exceptionResponse.getData().setMessage(ex.getMessage());
//        return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
//    }
//
////    @ExceptionHandler(UserNotFoundException.class)
////    public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
////        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
////                request.getDescription(false));
////        return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
////    }
//
////    @Override
////    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
////                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
////        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), "Validation Failed",
////                ex.getBindingResult().toString());
////        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
////    }
//}