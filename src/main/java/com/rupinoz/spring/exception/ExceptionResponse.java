package com.rupinoz.spring.exception;


public class ExceptionResponse {
    private String status;
    private String code;
    private ExceptionDataResponse data = new ExceptionDataResponse();

    public ExceptionResponse(String status, String code, ExceptionDataResponse data) {
        super();
        this.status = status;
        this.code = code;
        this.data = new ExceptionDataResponse();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ExceptionDataResponse getData() {
        return data;
    }

    public void setData(ExceptionDataResponse data) {
        this.data = data;
    }
}

